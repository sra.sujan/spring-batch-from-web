package com.esewa.springBatchDemo;

import com.esewa.springBatchDemo.service.BatchJob;
import com.esewa.springBatchDemo.service.Reader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.assertNotNull;

@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = BatchJob.class)
public class SpringBatchDemoApplicationTests {

	@Autowired
	private Reader reader;

	public StepExecution getStepExecution() {
		StepExecution execution = MetaDataInstanceFactory.createStepExecution();
		execution.getExecutionContext().putString("inputFilePath", "foo,bar,spam");
		return execution;
	}
	@Test
	public void testReader() {
		assertNotNull(reader);
	}

}

