package com.esewa.springBatchDemo.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "users")
public class Users{
    @Id
    private Long userId;

    private String name;

    private String department;

    private Integer account;

    @CreatedDate
    private Date createdDate = new Date();
}
