package com.esewa.springBatchDemo.utils;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceArrayPropertyEditor;

/**
 * author: Mohendra Amatya
 * readResources function takes a stagingDirectory
 *
 */
public class ResourceUtil {

    public static Resource[] readResources(String stagingDirectory, String pattern) {
        ResourceArrayPropertyEditor resourceLoader = new ResourceArrayPropertyEditor();
        resourceLoader.setAsText("file://" + stagingDirectory + "/*" + pattern + "*.csv");
        Resource[] resources = (Resource[]) resourceLoader.getValue();
        return resources;
    }

}