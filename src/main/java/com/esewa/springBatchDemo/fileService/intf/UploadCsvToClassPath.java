package com.esewa.springBatchDemo.fileService.intf;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface UploadCsvToClassPath {
    public void uploadFileToCsv(List<MultipartFile> file) throws Exception;
}
