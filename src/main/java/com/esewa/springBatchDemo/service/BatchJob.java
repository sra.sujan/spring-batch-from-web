package com.esewa.springBatchDemo.service;


import com.esewa.springBatchDemo.entity.Users;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableBatchProcessing
public class BatchJob {

    public static final String MULTI_RESOURCE_IMPORT_PERSON_STEP = "multiResourceImportPersonStep";
    public static final String MR_ITEM_READER = "mrItemReader";
    public static final String MR_ITEM_WRITER = "mrItemWriter";
    public static final String MR_ITEM_PROCESSOR = "mrItemProcessor";


    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final ItemValidator processor;
    private final SampleListener sampleListener;
    private final Writer writer;

    @Autowired
    public BatchJob(JobBuilderFactory jobBuilderFactory,
                    StepBuilderFactory stepBuilderFactory,
                    ItemValidator processor,
                    SampleListener sampleListener,
                    Writer writer) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.processor = processor;
        this.sampleListener = sampleListener;
        this.writer = writer;
    }

    @Bean
    public TaskExecutor taskExecutor(){
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    @Bean(name = "accountJob")
    public Job accountKeeperJob(@Qualifier(MULTI_RESOURCE_IMPORT_PERSON_STEP) Step step) {
        return  jobBuilderFactory.get("accounting-job")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }


    @Bean(name = MULTI_RESOURCE_IMPORT_PERSON_STEP)
    public Step step(@Qualifier(MR_ITEM_READER)Reader reader){
        return stepBuilderFactory.get("step-1")
                .listener(sampleListener)
                .<Users, Users> chunk(1000)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .taskExecutor(taskExecutor())
                .throttleLimit(20)
                .build();
    }

    @StepScope
    @Bean(name = MR_ITEM_READER)
    public Reader reader(@Value("#{jobParameters['inputFilePath']}") String filePath){
        return new Reader(filePath);
    }

   /* @Bean(name = MR_ITEM_PROCESSOR)
    public ItemValidator itemValidator() throws Exception {
        return new ItemValidator();
    }*/

}