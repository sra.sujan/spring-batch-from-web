package com.esewa.springBatchDemo.service;

import com.esewa.springBatchDemo.entity.Users;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class ItemValidator extends ValidatingItemProcessor<Users> {

    public ItemValidator() {
        CustomerValidator customerValidator = new CustomerValidator();
        setFilter(true);
        setValidator(customerValidator);
    }
}
