package com.esewa.springBatchDemo.service;

import com.esewa.springBatchDemo.entity.Users;
import com.esewa.springBatchDemo.repository.UsersRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class Writer implements ItemWriter<Users> {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Transactional
    public void write(List<? extends Users> users) throws Exception {
        usersRepository.saveAll(users);
    }
}
