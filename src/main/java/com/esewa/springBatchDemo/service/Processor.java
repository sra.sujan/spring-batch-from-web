/*
package com.esewa.springBatchDemo.service;

import java.util.Optional;

import com.esewa.springBatchDemo.entity.Users;
import com.esewa.springBatchDemo.repository.UsersRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Processor implements ItemProcessor<Users, Users> {

    @Autowired
    private UsersRepository userRepo;

    @Override
    public Users process(Users user) throws Exception {
        Optional<Users> userFromDb = userRepo.findById(user.getUserId());
        if(userFromDb.isPresent()) {
//            user.setAccount(user.getAccount().add(userFromDb.get().getAccount()));
            user.setName(userFromDb.get().getName().toUpperCase());
        }

        return user;
    }

}*/
